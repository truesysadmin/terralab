# Configure the GitLab Provider
provider "gitlab" {
#    token = "${var.gitlab_token}"
    token = "XqYJYeauLVJ-1o11B_Sz"
}


# Add a variable to the project
resource "gitlab_project_variable" "sample_project_variable" {
    project = "${gitlab_project.notebook_project.id}"
    key = "project_variable_key"
    value = "project_variable_value"
}

# Add a deploy key to the project
resource "gitlab_deploy_key" "sample_deploy_key" {
    project = "${gitlab_project.notebook_project.id}"
    title = "terraform example"
    key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDLUPRZ5PrSPEXb2E1t/d4C8202o+tkx3fssLz7aiOoFOpRWPo9Al5P7C2na2V9OWfCn///W3CSMKyYx8taEJpGvBg0mdwnX6bMftd3hOGkqj2kWT0qzwYpMvYsCdNe+CRJlix1XaLKNLem1lGXhQ5S2SHlvks0crAV4nA70Wdt3nIxkxx602Ghsb2mZ1CQprOyUPkWlCFxwvweCcSvAQwMCTlBiZulo21KmGvnfO1bjJgcM9US4N4mXdpHqV0hqsez41zV7RMX76rLRnPAXRm/UtWAQwvx23OdIHlqkNGcK33merDTeTm/lBQTYqhrgkrNH4PdCirCo4VTg1RohHC7 softserve\\drast@KAIRIKY"
}

# Add a group
resource "gitlab_group" "jupiter_group" {
    name = "Jupiter Example"
    path = "MyJupiter"
    description = "Jupiter example group"
}

# Add a project to the group - example/example
resource "gitlab_project" "notebook_project" {
    name = "Example project for notebook demo"
    namespace_id = "${gitlab_group.jupiter_group.id}"
}